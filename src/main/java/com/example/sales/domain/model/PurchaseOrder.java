package com.example.sales.domain.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class PurchaseOrder {
    @Id @GeneratedValue
    Long id;
}
