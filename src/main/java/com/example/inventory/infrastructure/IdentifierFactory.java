package com.example.inventory.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class IdentifierFactory {
    public String nextPlantInventoryEntryID() {
        return UUID.randomUUID().toString();
    }
}
